#!/bin/bash
set -x

source .stage.env


docker build --pull --tag ${AIRFLOW_IMAGE_NAME}:${AIRFLOW_IMAGE_TAG} .
docker tag ${AIRFLOW_IMAGE_NAME}:${AIRFLOW_IMAGE_TAG} gcr.io/$project_id/${AIRFLOW_IMAGE_NAME}:${AIRFLOW_IMAGE_TAG}
docker push gcr.io/$project_id/${AIRFLOW_IMAGE_NAME}:${AIRFLOW_IMAGE_TAG}