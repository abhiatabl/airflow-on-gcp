#!/bin/bash
set -x

source .stage.env

helm upgrade \
    --install $RELEASE_NAME \
    --namespace $NAMESPACE  \
    apache-airflow/airflow \
    -f override-values.yaml \

# Release "airflow" has been upgraded. Happy Helming!
# NAME: airflow
# LAST DEPLOYED: Tue Mar 29 00:51:38 2022
# NAMESPACE: airflow
# STATUS: deployed
# REVISION: 13
# TEST SUITE: None
# NOTES:
# Thank you for installing Apache Airflow 2.2.3!

# Your release is named airflow.
# You can now access your dashboard(s) by executing the following command(s) and visiting the corresponding port at localhost in your browser:

# Airflow Webserver:     kubectl port-forward svc/airflow-webserver 8080:8080 --namespace airflow
# Flower dashboard:      kubectl port-forward svc/airflow-flower 5555:5555 --namespace airflow
# Default Webserver (Airflow UI) Login credentials:
#     username: admin
#     password: admin
# Default Postgres connection credentials:
#     username: postgres
#     password: postgres
#     port: 5432

# You can get Fernet Key value by running the following:

#     echo Fernet Key: $(kubectl get secret --namespace airflow airflow-fernet-key -o jsonpath="{.data.fernet-key}" | base64 --decode)

# ###########################################################
# #  WARNING: You should set a static webserver secret key  #
# ###########################################################

# You are using a dynamically generated webserver secret key, which can lead to
# unnecessary restarts of your Airflow components.

# Information on how to set a static webserver secret key can be found here:
# https://airflow.apache.org/docs/helm-chart/stable/production-guide.html#webserver-secret-key